<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">

			
			<div class="row" style="justify-content: center;">
							
				<label for="location">Location</label>
				<input type="checkbox" class="checkbox" id="location">
				<label for="branch">Branch</label>
				<input type="checkbox" class="checkbox" id="branch">
				
			</div>

			<div>
				<h5>Description</h5>
				<p class="description"><?php  //var_dump(get_option('offices_description')); ?></p>
			</div>

			<div class="row" style="margin-top: 50px;">
				<h4>List offices</h4>

				<?php 

				$args = array(
					'post_per_page' => -1,
				    'post_type'=> 'office',
				    'order'    => 'ASC'
				    );              

				$the_query = get_posts($args);

					$output = '<ul class="list">';
					foreach ($the_query as $key => $query) {
						$output .= '<li>';
						$output .= $query->post_title;
						$output .= '</li>';
					}
					$output .= '</ul>';
					echo $output;

				wp_reset_query();


				?>

			</div>


</div><!-- .wrap -->

<?php get_footer();
