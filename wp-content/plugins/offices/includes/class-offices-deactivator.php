<?php

/**
 * Fired during plugin deactivation
 *
 * @link       example.com
 * @since      1.0.0
 *
 * @package    Offices
 * @subpackage Offices/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Offices
 * @subpackage Offices/includes
 * @author     Goran <go_jov@yahoo.com>
 */
class Offices_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
