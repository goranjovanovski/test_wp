<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       example.com
 * @since      1.0.0
 *
 * @package    Offices
 * @subpackage Offices/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Offices
 * @subpackage Offices/admin
 * @author     Goran <go_jov@yahoo.com>
 */
class Offices_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * The option name of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $option_name = 'offices';

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Offices_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Offices_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/offices-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Offices_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Offices_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/offices-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Add an options page 
	 *
	 * @since  1.0.0
	 */

	public function add_options_page(){

		$this->plugin_screen_hook_suffix = add_options_page(
			__('Location and branches settings', 'offices'),
			__('Location and branches', 'offices'),
			'manage_options',
			$this->plugin_name,
			array ($this, 'display_options_page')
		);
	}

	/**
	 * Render options page
	 *
	 * @since  1.0.0
	 */

	public function display_options_page(){
		include_once 'partials/offices-admin-display.php';
	}

	/**
	 * Register the plugin settings fields
	 *
	 * @since  1.0.0
	 */

	public function register_setting(){
	
		add_settings_section(
			$this->option_name . '_general',
			__('General', 'offices'),
			[$this, $this->option_name . '_general'],
			$this->plugin_name
		);

		add_settings_field(

			$this->option_name . '_description_fields',
			__( 'Description for branches and locations', 'offices' ),
			array( $this, $this->option_name . '_description_fields' ),
			$this->plugin_name,
			$this->option_name . '_general',
			array( 'label_for' => $this->option_name . '_description_fields' )
		);

		register_setting( $this->plugin_name, $this->option_name . '_description', array( $this, $this->option_name . '_sanitize_description' ) );
	}

	/**
	 * Render the paragraph field for description option
	 *
	 * @since  1.0.0
	 */

	public function offices_general(){
		echo '<p> Fill the textarea fields with appropriate description <p>';
	}

	/**
	 * Render the textarea for description option
	 *
	 * @since  1.0.0
	 */

	public function offices_description_fields(){
		$description = get_option($this->option_name .'_description');
		?>
	<!-- 	<fieldset>
			<i class="fas fa-code-branch"></i>

			<?php //for ($i=0; $i<count(get_option($this->option_name .'_description'));$i++) { ?>

				<?php 
					// echo '<label for="'. $this->option_name .'_description_'.$i .' ">';
					// if($i == 0) :
					// 	_e( 'Description for Branches', 'offices' );
					// endif;
					// if($i == 1) :
					// 	_e( 'Description for Locations', 'offices' );
					// endif;
					// echo '</label>';
				 ?>
			<!-- <p>					 -->
				<!-- <textarea name="<?php //echo $this->option_name .'_description[]' ?>" id="<?php// echo $this->option_name .'_description_'.$i ?>" cols="60" rows="10"><?php //echo get_option($this->option_name .'_description')[$i] ?></textarea> -->
			<!-- </p> -->
			<!-- <br> -->
			<?php //} ?>	

		<!-- </fieldset> -->
			<fieldset>
			<i class="fas fa-code-branch"></i>

			<label for="<?= $this->option_name .'_description' ?>">
				<?php _e( 'Description for Branches', 'offices' ); ?>
			</label>
			
			<p>					
				<textarea name="<?= $this->option_name .'_description[]' ?>" id="<?= $this->option_name ?>" cols="60" rows="10"><?= get_option($this->option_name .'_description')[0] ?></textarea>
			</p>
			<br>
			<label for="<?= $this->option_name .'_description' ?>">
				<?php _e( 'Description for Locations', 'offices' ); ?>
			<p>			
				<textarea name="<?= $this->option_name .'_description[]' ?>" id="<?= $this->option_name ?>" cols="60" rows="10"><?= get_option($this->option_name .'_description')[1] ?></textarea>
			</p>	
			</label>

			<br>
			<label for="<?= $this->option_name .'_description' ?>">
				<?php _e( 'Description for Branches and Locations', 'offices' ); ?>
			</label>
			<p>			
				<textarea name="<?= $this->option_name .'_description[]' ?>" id="<?= $this->option_name ?>" cols="60" rows="10"><?= get_option($this->option_name .'_description')[2] ?></textarea>
			</p>		

		</fieldset>
			<?php
	}

	protected function offices_sanitze_descrition($description){
		esc_textarea( $description );
	}

	/**
	* Creates a new custom post type
	*
	* @since 1.0.0
	* @access public
	* @uses register_post_type()
	*/


	public function office_cpt(){

		$cap_type = 'post';
		$plural = 'Offices';
		$single = 'Office';
		$cpt_name = 'office';

		$opts['can_export'] = true;
		$opts['capability_type'] = $cap_type;
		$opts['description'] = '';
		$opts['exclude_from_search'] = false;
		$opts['has_archive'] = true;
		$opts['hierarchical'] = false;
		$opts['map_meta_cap'] = true;
		$opts['menu_icon'] = 'dashicons-building';
		$opts['menu_position'] = 5;
		$opts['public'] = true;
		$opts['publicly_querable'] = true;
		$opts['query_var'] = true;
		$opts['register_meta_box_cb'] = '';
		$opts['rewrite'] = false;
		$opts['show_in_admin_bar'] = true;
		$opts['show_in_menu'] = true;
		$opts['show_in_nav_menu'] = true;
		 
		$opts['labels']['add_new'] = esc_html__( "Add New {$single}" );
		$opts['labels']['add_new_item'] = esc_html__( "Add New {$single}" );
		$opts['labels']['all_items'] = esc_html__( $plural );
		$opts['labels']['edit_item'] = esc_html__( "Edit {$single}"  );
		$opts['labels']['menu_name'] = esc_html__( $plural );
		$opts['labels']['name'] = esc_html__( $plural );
		$opts['labels']['name_admin_bar'] = esc_html__( $single );
		$opts['labels']['new_item'] = esc_html__( "New {$single}" );
		$opts['labels']['not_found'] = esc_html__( "No {$plural} Found" );
		$opts['labels']['not_found_in_trash'] = esc_html__( "No {$plural} Found in Trash" );
		$opts['labels']['parent_item_colon'] = esc_html__( "Parent {$plural} :" );
		$opts['labels']['search_items'] = esc_html__( "Search {$plural}" );
		$opts['labels']['singular_name'] = esc_html__( $single );
		$opts['labels']['view_item'] = esc_html__( "View {$single}" );
		
		register_post_type( strtolower( $cpt_name ), $opts );
	} 


	public function create_location_type_taxonomies() {

		$labels = array(
			'name' => _x( 'Locations', 'office'),
			'singular_name' => _x( 'Location', 'office' ),
			'search_items' =>  __( 'Search Location' ),
			'all_items' => __( 'All Locations' ),
			'parent_item' => __( 'Parent Genre' ),
			'parent_item_colon' => __( 'Parent Genre:' ),
			'edit_item' => __( 'Edit Location' ),
			'update_item' => __( 'Update Location' ),
			'add_new_item' => __( 'Add New Location' ),
			'new_item_name' => __( 'New Location Name' ),
		); 	
		register_taxonomy( 'location_type', array( 'office' ), array(
			'hierarchical' => true,
			'labels' => $labels,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'location' ),
		));
	
	}

	public function create_branch_type_taxonomies() {

		$labels = array(
			'name' => _x( 'Branches', 'office'),
			'singular_name' => _x( 'Branch', 'office' ),
			'search_items' =>  __( 'Search Branch' ),
			'all_items' => __( 'All Branches' ),
			'parent_item' => __( 'Parent Genre' ),
			'parent_item_colon' => __( 'Parent Genre:' ),
			'edit_item' => __( 'Edit Branch' ),
			'update_item' => __( 'Update Branch' ),
			'add_new_item' => __( 'Add New Branch' ),
			'new_item_name' => __( 'New Branch Name' ),
		); 	
		register_taxonomy( 'branch_type', array( 'office' ), array(
			'hierarchical' => true,
			'labels' => $labels,
			'show_ui' => true,
			'query_var' => true,
			'rewrite' => array( 'slug' => 'branch' ),
		));
	
	}
	

}
